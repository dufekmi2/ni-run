#include "ast_interpreter.h"

Type * interpret_ast(Ast * ast, Definitions * definitions){
	switch(ast->kind){
	case AST_NULL:
		return make_null();
	case AST_BOOLEAN:
		return make_boolean((AstBoolean*)ast);
	case AST_INTEGER:
		return make_integer((AstInteger*)ast);
	case AST_ARRAY:
		NOT_IMPLEMENTED;
		break;
	case AST_OBJECT:
		NOT_IMPLEMENTED;
		break;
	case AST_FUNCTION:
		NOT_IMPLEMENTED;
		break;
	case AST_DEFINITION:
		new_definition((AstDefinition*)ast,definitions);
		break;
	case AST_VARIABLE_ACCESS:
        return seek_definition((AstVariableAccess*)ast,definitions);
	case AST_VARIABLE_ASSIGNMENT:
		NOT_IMPLEMENTED;
		break;
	case AST_INDEX_ACCESS:
		NOT_IMPLEMENTED;
		break;
	case AST_INDEX_ASSIGNMENT:
		NOT_IMPLEMENTED;
		break;
	case AST_FIELD_ACCESS:
		NOT_IMPLEMENTED;
		break;
	case AST_FIELD_ASSIGNMENT:
		NOT_IMPLEMENTED;
		break;
	case AST_FUNCTION_CALL:
		printf("AST_FUNCTION_CALL\n");
		NOT_IMPLEMENTED;
		break;
	case AST_METHOD_CALL:
		printf("AST_METHOD_CALL\n");
		NOT_IMPLEMENTED;
		break;
	case AST_CONDITIONAL:
		NOT_IMPLEMENTED;
		break;
	case AST_LOOP:
		NOT_IMPLEMENTED;
		break;
	case AST_PRINT:;
		AstPrint * astPrint = (AstPrint*)ast;
		print_ast(astPrint,definitions);
		return make_null();
	case AST_BLOCK:;
		AstBlock * astBlock = (AstBlock*) ast;
		printf("Scoping not implemented, using global scope\n");
		for(int i = 0;i<astBlock->expression_cnt-1;++i){
			interpret_ast(astBlock->expressions[i],definitions);
		}
		return interpret_ast(astBlock->expressions[astBlock->expression_cnt-1],definitions);
	case AST_TOP:;
		AstTop * astTop = (AstTop*)ast;
		for(int i = 0;i<astTop->expression_cnt;++i){
			interpret_ast(astTop->expressions[i],definitions);
		}
		return 0;
	}
}

void print_ast(AstPrint * astPrint,Definitions* definitions){
    char * string = (char*)malloc(astPrint->format.len);
    memcpy(string,astPrint->format.str,astPrint->format.len);
    int length = astPrint->format.len;
    int pos = 0;
    int argument_index = 0;
    
    char* back_slash_pos = strstr(string,"\\");
    char* tilda_pos = strstr(string,"~");
    for(;;){
        if(tilda_pos == NULL && back_slash_pos == NULL){
            printf("%.*s",(length-pos),string);
            break;
        }
        if((tilda_pos == NULL || back_slash_pos < tilda_pos) && back_slash_pos != NULL){
            printf("%.*s",(int)(back_slash_pos-string),string);
            pos +=(int)(back_slash_pos-string)+2;
            string = back_slash_pos+2;
            if(*(back_slash_pos+1)=='n'){
                printf("\n");
            }else if(*(back_slash_pos+1)=='t'){
                printf("\t");
            }else if(*(back_slash_pos+1)=='\\'){
                printf("\\");
            }else if(*(back_slash_pos+1)=='r'){
                printf("\r");
            }else if(*(back_slash_pos+1)=='"'){
                printf("\"");
            }else if(*(back_slash_pos+1)=='~'){
                printf("~");
                tilda_pos = strstr(string,"~");
            }
            fflush(stdin);
            back_slash_pos = strstr(string,"\\");
        }else if (tilda_pos != NULL){
            printf("%.*s",(int)(tilda_pos-string),string);
            
            print_runtime_object(interpret_ast(astPrint->arguments[argument_index],definitions));
            argument_index++;
            pos+=(int)(tilda_pos-string)+1;
            string+=(int)(tilda_pos-string)+1;
            tilda_pos = strstr(string,"~");
        }
    }
}

void print_runtime_object(Type * value){
    switch(value->kind){
            case AST_INTEGER:
                printf("%d",value->integer);
                break;
            case AST_BOOLEAN:;
                if(value->boolean)
                    printf("true");
                else
                    printf("false");
                break;
            case AST_NULL:
                printf("NULL");
                break;
            case AST_ARRAY:
                /*AstArray *array = (AstArray*)ast;
                printf("[");
                for(int o = 0;o<array->size;++o){
                    //print_runtime_object(array->)//
                    printf(",");
                }
                printf("]");*/
                break;
            case AST_VARIABLE_ACCESS:
                break;
    }
}

Definitions * init_definitions(){
    Definitions * definition = (Definitions*)malloc(sizeof(Definitions));
    definition->keys = (Str*)malloc(sizeof(Str));
    definition->values = (Type**)malloc(sizeof(Type*));
    definition->size = 0;
    definition->capacity = 1;
}


Type * seek_definition(AstVariableAccess * ast,Definitions * definition){
    for(int i = 0;i<definition->size;++i){
        if(str_eq(ast->name,definition->keys[i])){
            return definition->values[i];
        }
    }
    return NULL;
}

Type * new_definition(AstDefinition * ast,Definitions * definitions){
    if(definitions->size == definitions->capacity){
        definitions->keys = realloc(definitions->keys,sizeof(Str)*definitions->capacity*2);
        definitions->capacity *= 2;
    }
    
    Type * type = interpret_ast(ast->value,definitions);
    
    definitions->keys[definitions->size].str = (u8*)malloc(ast->name.len);
    memcpy(definitions->keys[definitions->size].str,ast->name.str,ast->name.len);
    definitions->keys[definitions->size].len = ast->name.len;
    
    definitions->values[definitions->size] = type;
    
    definitions->size++;
}


Type * make_null(){
    Type * null = (Type*)malloc(sizeof(Type));
    null->kind = AST_NULL;
    return null;
}

Type * make_integer(AstInteger * ast){
    Type * integer = (Type*)malloc(sizeof(Type));
    integer->integer = ast->value;
    integer->kind = AST_INTEGER;
    return integer;
}

Type * make_boolean(AstBoolean * ast){
    Type * boolean = (Type*)malloc(sizeof(Type));
    boolean->boolean = ast->value;
    boolean->kind = AST_BOOLEAN;
    return boolean;
}
