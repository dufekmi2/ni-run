#pragma once

#include <stdio.h>
#include "parser.h"
#include "arena.h"
#include "helpers.h"

typedef struct{
    AstKind kind;
    union{
        bool boolean;
        int integer;
        Ast * object;
    };
}Type;

typedef struct{
    size_t size;
    size_t capacity;
    Str * keys;
    Type ** values;
}Definitions;

Type * interpret_ast(Ast * ast, Definitions * definitions);

Type * new_definition(AstDefinition*,Definitions*);

Type * make_null();
Type * make_integer(AstInteger * ast);
Type * make_boolean(AstBoolean * ast);

typedef struct{
    Definitions * definitions;
}Enviroment;

void print_ast(AstPrint * astPrint,Definitions * definitions);
void print_runtime_object(Type * ast);

Definitions * init_definitions();


void add_definition(Definitions * definitions,Str * name,Type * ast);
Type * seek_definition(AstVariableAccess * ast,Definitions * definition);

