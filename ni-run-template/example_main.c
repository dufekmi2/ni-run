#include <stdio.h>

#include "arena.h"
#include "parser.h"
#include "helpers.h"
#include "ast_interpreter.h"



void start_ast_interpreter(Ast * ast){
	Definitions * definitions = init_definitions();
	interpret_ast(ast,definitions);
}

int main(int argc, char **argv) {
#ifdef _WIN32
	// Set standard output mode to "binary" on Windows.
	// https://nullprogram.com/blog/2021/12/30/
	int _setmode(int, int);
	_setmode(1, 0x8000);
#endif
	Str source;
	Str file_name;

	if (argc < 2) {
		fprintf(stderr, "Error: expected at least one argument\n");
		return 1;
	}
	
	if(strcmp(argv[1],"run")==0){
		for(int i = 2;i<argc;i+=2){
			if(str_eq(STR(argv[i]),STR("--heap-size"))){
				printf("Not implemented\n");
				//heap_size = argv[i+1];
			}
			else if(str_eq(STR(argv[i]),STR("--heap-log"))){
				printf("Not implemented\n");
				//heap_log = argv[i+1];
			}
			else{
				FILE * f = fopen(argv[i],"rb");
				if(f == NULL){
					printf("File could not be opened\n");
					return 1;
				}
				fseek(f,0,SEEK_END);
				size_t pos = ftell(f);
				char * file_contents = (char*)malloc(pos);
				rewind(f);
				size_t loaded = fread(file_contents,pos,1,f);
				source.str = file_contents;
				source.len = strlen(file_contents)-1;
				
				break;
			}
			
		}
	}
	else if(str_eq(STR(argv[1]),STR("ast_interpret"))){
		file_name = STR(argv[2]);
		printf("Not implemented\n");
		return 2;
	}
	else{
		source = (Str) { .str = (u8 *) argv[1], .len = strlen(argv[1]) };
		printf("input = \"%s\"\n",source.str);
	}
	Arena arena;
	arena_init(&arena);
	//source = STR("print(\"5\")");
	Ast *ast = parse_src(&arena, source);

	if (ast == NULL) {
		fprintf(stderr, "Failed to parse source\n");
		arena_destroy(&arena);
		return 1;
	}
	
	start_ast_interpreter(ast);

	arena_destroy(&arena);

	return 0;
}
